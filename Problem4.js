function listOfCarYears(inventory) {
    let carYears = [];
    if (typeof inventory !== 'object' || inventory.length === 0) {
        return [];
    }
    for (let i=0; i < inventory.length; i++) {
        carYears.push(inventory[i].car_year);
    }
    return carYears;
}

module.exports = listOfCarYears;