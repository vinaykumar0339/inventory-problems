function listOfCarModels(inventory) {
    let carModels = [];
    
    if (typeof inventory !== 'object' || inventory.length === 0) {
        return [];
    }

    for (let i=0; i < inventory.length; i++) {
        carModels.push(inventory[i].car_model);
    }

    return carModels.sort();
}

module.exports = listOfCarModels;