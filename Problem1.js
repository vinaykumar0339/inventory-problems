function findById(inventory, id) {

    // this works for -> if we doesn't give any input it gives empty array
    if (!id || typeof inventory !== 'object' || inventory.length === 0) {
        return [];
    }

    for (let i=0; i < inventory.length; i++) {
        let inven = inventory[i];
        if (inven.id === id) {
            return inven;
        }
    }
    return [];
}

module.exports = findById;
