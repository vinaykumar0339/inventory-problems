function BmwAndAudi(inventory) {
    let bmwAndAudi = [];
    if (typeof inventory !== 'object' || inventory.length === 0) {
        return [];
    }

    for (let i=0; i< inventory.length; i++) {
        let inven = inventory[i];
        if (inven.car_make === 'BMW' || inven.car_make === 'Audi') {
            bmwAndAudi.push(inven);
        }else{
            continue;
        }
    }
    return JSON.stringify(bmwAndAudi);
}

module.exports = BmwAndAudi;