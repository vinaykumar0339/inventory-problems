function findLastInventory(inventory) {
    if (typeof inventory !== 'object' || inventory.length === 0) {
        return [];
    }
    return inventory[inventory.length - 1];
}

module.exports = findLastInventory;